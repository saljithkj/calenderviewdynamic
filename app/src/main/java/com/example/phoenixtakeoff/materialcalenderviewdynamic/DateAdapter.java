package com.example.phoenixtakeoff.materialcalenderviewdynamic;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.zip.Inflater;

class DateAdapter extends RecyclerView.Adapter<DateAdapter.Viewholder> {

ArrayList<String> selecteddates;
    public DateAdapter(ArrayList<String> selecteddates) {
    this.selecteddates=selecteddates;
    }

    @NonNull
    @Override
    public DateAdapter.Viewholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.carddates, viewGroup, false);

        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DateAdapter.Viewholder viewholder, int i) {

        viewholder.textView.setText(selecteddates.get(i));

    }

    @Override
    public int getItemCount() {
        return selecteddates.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        TextView textView;

        public Viewholder(@NonNull View itemView) {
            super(itemView);


            textView=itemView.findViewById(R.id.textViewName);
        }
    }
}
