package com.example.phoenixtakeoff.materialcalenderviewdynamic;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.annimon.stream.Stream;
import com.applandeo.materialcalendarview.CalendarUtils;
import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.DatePicker;
import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.builders.DatePickerBuilder;
import com.applandeo.materialcalendarview.listeners.OnDayClickListener;
import com.applandeo.materialcalendarview.listeners.OnSelectDateListener;
import com.applandeo.materialcalendarview.utils.DateUtils;

import java.text.SimpleDateFormat;
import java.time.MonthDay;
import java.util.Calendar;


import java.util.ArrayList;

import java.util.Date;
import java.util.List;


public class MainActivity extends AppCompatActivity {
CalendarView calendarView;
ArrayList<String> selecteddates;
RecyclerView recyclerView;
LinearLayoutManager linearLayoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
         calendarView =  findViewById(R.id.calendarView);
recyclerView=findViewById(R.id.recycler);

        recyclerView.setHasFixedSize(true);

        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        List<EventDay> events = new ArrayList<>();

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, 7);
selecteddates=new ArrayList<>();

       calendarView.setOnDayClickListener(new OnDayClickListener() {
           @Override
           public void onDayClick(EventDay eventDay) {

               Calendar clickedDayCalendar = eventDay.getCalendar();


               Date date= clickedDayCalendar.getTime();
               SimpleDateFormat format1 = new SimpleDateFormat("dd-MM-yyyy");
               String date1 = format1.format(date);
               Toast.makeText(MainActivity.this, String.valueOf(date1), Toast.LENGTH_SHORT).show();
               selecteddates.add(date1);

               DateAdapter adapter=new DateAdapter(selecteddates);
               recyclerView.setAdapter(adapter);
           }
       });

        calendarView.setSelectedDates(getSelectedDays());


    }

    private List<Calendar> getSelectedDays() {
        List<Calendar> calendars = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            Calendar calendar = DateUtils.getCalendar();
            calendar.add(Calendar.DAY_OF_MONTH, i);


        }
        Log.e("Callender",calendars.toString());

        return calendars;
    }
}
